// Operators

// Assignment operators
// Basic assignment operator
let assignmentNumber = 8;

// Arithmetic Assignment operators
// Addition assignment operators (+=)

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber +=2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction assignment opeators (-=)
assignmentNumber -= 2; 
console.log("Result of subtraction assignment operator: " + assignmentNumber) ;

// Multiplication assignment opeators (*=)
assignmentNumber *= 2; 
console.log("Result of multiplication assignment operator: " + assignmentNumber) ;

// Division assignment opeators (/=)
assignmentNumber /= 2; 
console.log("Result of division assignment operator: " + assignmentNumber) ;

//Arithmetic Operators
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Results of addition operator: " + sum);

let difference = x - y;
console.log("Results of subtraction operator: " + difference);

let product = x * y;
console.log("Results of multiplication operator: " + product);

let quotient = x / y ;
console.log("Results of division operator: " + quotient);

let remainder = x % y ;
console.log("Results of division operator: " + remainder);

// Multiple Operators and Parentheses

// Using MDAS
let mdas = 1 + 2 - 3 * 4 / 5;
console.log ("Results of mdas operations: " + mdas);

// Using Parentheses
let pemdas =  1 + ( 2 - 3) * (4 / 5);
console.log ("Results of pemdas operations: " + pemdas);

// Increment (++) and decrement (--)
let z = 1;
// Pre-increment
let increment = ++z;
console.log("Results of pre-increment: " + increment);
console.log("Results of pre-increment: " + z);

// Post-increment
increment = z++;
console.log("Results of post-increment: " + increment);
console.log("Results of post-increment: " + z);

// Pre-decrement
let decrement = --z;
console.log("Results of pre-decrement: " + decrement);
console.log("Results of pre-decrement: " + z);

// Post-decrement
decrement = z--;
console.log("Results of post-decrement: " + decrement);
console.log("Results of post-decrement: " + z);

// Type coercion

let numA = '10' ;
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = true + 1;
console.log(numC);

let numD = false + 1;
console.log(numD);

// Relational operators

// Equality operators (==) (conmpare then true or false)

let juan = 'juan';
console.log ( 1 ==1 );
console.log ( 1 ==2 );
console.log (1 =='1');
console.log ('juan' == juan); 

// Inequality operator (!=)

console.log ( 1 !=1 ); //false
console.log ( 1 !=2 ); //true
console.log (1 !='1'); //false
console.log ('juan' != juan); //true

//  < , >, <= , >=
console.log("Less than and greater than")
console.log (4 < 6); //true
console.log (2 > 8); //false
console.log (5 >= 5); //true
console.log (10 <= 15); //true

// Strict equality operator (===)
console.log("Strict equality")
console.log (1 === '1'); //false

// Strict equality operator (!==)
console.log("Strict inequality")
console.log (1 !== '1'); //true

// Logical operators
let isLegalAge = true;
let isRegistered = false;

// And Operator (&&)
// Returnns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Results of logical AND operator: " + allRequirementsMet);

// OR Operator (||)
// Return true if one the operands is true
let someRequirmentsMet = isLegalAge || isRegistered;
console.log("Results of logical OR operator: " + someRequirmentsMet);

//Selection control structures

// if,else if and else statement

// if statement
// Executes a statement if a specified condition is true
/*Syntax:
	if(condition){
		statement/s;
	}
*/

let numE = -1;

if(numE < 0){
	console.log("Hello");
}

let nickName = 'Matteo';

if(nickName == 'Matt'){
	console.log("Hello " + nickName);
}

// else if clause 
// executes a statement if previous conditions are false and if the specified condition is true
/*Syntax:
	if(condition1){
		statement/s;
		}
		else if(condition2){
			statement/s
		}
		else if(condition3){
			statement/s
		}
		else if(conditionN){
			statement/s
		}
*/

let numF = 1;

if(numE > 0){
	console.log("Hello");
}
else if(numF > 0){
	console.log("World");
}

//else clause
//execute a statement if all other conditions are not met.
/*Syntanx
	if(condition1){
		statement/s;
		}
		else if(condition2){
			statement/s
		}
		else if(condition3){
			statement/s
		}
		else if(conditionN){
			statement/s
		}
		else {
			statement/s
*/
/*
	numE = -1;
	numF = 1;
*/

if(numE > 0){
	console.log("Hello");
}
else if(numF === 0){
	console.log("World");
}
else{
	console.log("Again");
}

let message = 'No message';
console.log(message);

// range (&&)

function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30){
		return 'Not a typhoon yet.';	
	}
	else if(windSpeed >= 31 && windSpeed <=60){
		return 'Tropical depression detected';
	}
	else if(windSpeed >= 62 && windSpeed <=88){
		return 'Tropical depression detected';
	}
	else if (windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected';
	}
	else{
		return 'Typhoon detected';
	}
}

message = determineTyphoonIntensity(68)
console.log(message);

/*let department = prompt("Enter your department");

if (department === 'a' || department 'A'){
	console.log("You are in department A");
}
else if (department === 'b' || department 'B'){
	console.log("You are in department B");
} 
else{
	console.log("Invalid department");
}
*/
//Ternary Operator
/*Syntax
	(expression) ? ifTrue : ifFalse;
*/

let ternaryResult = ( 1 < 18) ? "true" : "false";
console.log("Result of Ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge(){
	name = 'John'
	return 'You are of the legal age limit' ;
}

function isUnderAge(){
	name = 'Jane'
	return 'You are under the legal age limit' ;
}

// parseInt converts string values into numeric types
let age = parseInt(prompt("what is your age?"));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log('Result of Ternary Operator in functions:' + legalAge + ', ' + name); 

// kasama ung name malalagay
/*let age = prompt("what is your age?");
console.log(age);

let names = prompt("what is your name?");
console.log(names);


let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log('Result of Ternary Operator in functions:' + legalAge + ', ' + names);*/

// Switch Statement
/* Syntax
	switch (expression){
	case value1:
		statement;
		break;
	case value2:
		statement;
		break;
	case value3:
		statement;
		break;
	case valueN:
		statement;
		break;
	default:
		statement;	
	}
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day){
	case 'sunday':
		console.log("The color of the day is red");
		break;
	case 'monday':
		console.log("The color of the day is orange");
		break;
	case 'tuesday':
		console.log("The color of the day is yellow");
		break;
	case 'wednesday':
		console.log("The color of the day is green");
		break;
	case 'thursday':
		console.log("The color of the day is blue");
		break;
	case 'friday':
		console.log("The color of the day is indigo");
		break;
	case 'saturday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day")

}

//Try-Catch-Finally Statement

function showIntensityAlert(windSpeed){
	try {
		// Attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed))
	}
	// error/err are comonly used variable by developers for storing errors
	catch (error){
		console.log(typeof error);

		console.warn(error.message);
		// error.message is used to access the information relating to an error object	
	}
	finally{
		// continue on executing the code regardless of sucess or failure of code execution in the 'try' block.
		alert ('Intensity update will show me alert.')
	}
}

showIntensityAlert(56);